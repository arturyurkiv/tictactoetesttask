﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Exit_b : MonoBehaviour
 {

	private void Start ()
	{
		var exitButton = GetComponent<Button>();
		exitButton.onClick.AddListener(QuitGame);
	}

	private void QuitGame()
	{
		Application.Quit();
	}
	
	
		
	
}
