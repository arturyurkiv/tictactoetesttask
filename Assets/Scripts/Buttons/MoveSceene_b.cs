﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MoveSceene_b : MonoBehaviour
{

	[SerializeField] private Scenes _scene;

	private void Start ()
	{
		var currentButton = GetComponent<Button>();
		currentButton.onClick.AddListener(StartGame);
	}

	private void StartGame()
	{
		SceneManager.LoadScene((int)_scene);
		
	}
}
