﻿ using System.Collections;
using System.Collections.Generic;
 using System.Net.Mime;
 using UnityEngine;
using UnityEngine.UI;

public class Plane : MonoBehaviour
{

    [SerializeField] private int _row;
    [SerializeField] private int _col;


    private void Start()
    {
        var _planeButton = GetComponent<Button>();
        _planeButton.onClick.AddListener(MakeMove);

    }

    private void MakeMove()
    {
        var gamelogic = GameObject.Find("GameManager").GetComponent<GameLogic>();

        gamelogic.GetPlayerInput(_row, _col);

    }

    public void ChangeText(string planeText)
    {
        transform.GetChild(0).GetComponent<Text>().text = planeText;
    }
    

    public int GetRow()
    {
        return _row;
    }
    
    public int GetCol()
    {
        return _col;
    }
    
    

    
}
