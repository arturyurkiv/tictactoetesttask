﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net.Mime;
using UnityEditor;
using UnityEngine.UI;


public class GameLogic : MonoBehaviour
{
    [SerializeField] private GameObject[] _planes;

    private int[,] _board;

    private const int _boardSize = 3;
    private int _maxDepth;


    private int _row = 3;
    private int _col = 3;

    private int _bestRow;
    private int _bestCol;
    private int _score;
    private int _totalMoves = 0;
    private int _win;

    //id of computer
    private int _computerID = 1;


    private int _currentPlayer = 1;

    private void Start()
    {
        _board = new int[_boardSize, _boardSize];
        InitializeGame();
    }

    private void Update()
    {
        if (_totalMoves == 8)
        {
            Settings.isTie = true;
            OpenEndingMenu(0);
        }

        _win = CheckForWinner(_board);
        if (_win != 0)
        {
            if (_win != _computerID)
            {
                Settings.isWin = true;
                Settings.goFirst = true;
                Settings.isTie = false;
            }
            else
            {
                Settings.isWin = false;
                Settings.isTie = false;
            }

            OpenEndingMenu(_win);
        }


        if (_currentPlayer == _computerID)
        {
            _score = MiniMax(_board, _currentPlayer, true, 0);

            if (_score != -1000)
            {
                _row = _bestRow;
                _col = _bestCol;
            }

            CheckBoardIsEmpty();
        }
    }

    private void InitializeGame()
    {
        for (int i = 0; i < _boardSize; i++)
        {
            for (int j = 0; j < _boardSize; j++)
            {
                _board[i, j] = 0;
            }
        }

        if (!Settings.isWin && !Settings.isTie)
        {
            _currentPlayer = 1;
        }
        else if (Settings.isWin && !Settings.isTie)
        {
            _currentPlayer = -1;
        }
        else
        {
            if (Settings.goFirst)
            {
                _currentPlayer = 1;
            }
            else
            {
                _currentPlayer = -1;
            }
        }
        _maxDepth = GameDifficult.AiDifficult;

        _bestRow = -1000;
        _bestCol = -1000;
    }

    private int MiniMax(int[,] hypotheticalBoard, int player, bool playerMove, int depth)
    {
        if (depth > _maxDepth)
            return 0;

        var winner = CheckForWinner(hypotheticalBoard);
        if (winner != 0)
        {
            return winner;
        }

        int moveRow = -1;
        int moveCol = -1;

        if (playerMove)
        {
            _score = -2;
        }
        else
        {
            _score = 2;
        }

        for (int i = 0; i < _boardSize; i++)
        {
            for (int j = 0; j < _boardSize; j++)
            {
                if (hypotheticalBoard[i, j] == 0)
                {
                    hypotheticalBoard[i, j] = player;
                    int thisScore = MiniMax(hypotheticalBoard, -1 * player, !playerMove, depth + 1);

                    if (playerMove)
                    {
                        if (thisScore > _score)
                        {
                            _score = thisScore;
                            moveRow = i;
                            moveCol = j;
                        }
                    }
                    else
                    {
                        if (thisScore < _score)
                        {
                            _score = thisScore;
                            moveRow = i;
                            moveCol = j;
                        }
                    }

                    hypotheticalBoard[i, j] = 0;
                }
            }
        }

        if (moveRow == -1) return 0;
        _bestRow = moveRow;
        _bestCol = moveCol;
        return _score;
    }

    private int CheckForWinner(int[,] board)
    {
        int prev, winner;

        for (int i = 0; i < _boardSize; i++)
        {
            prev = board[i, 0];
            winner = prev;

            for (int j = 1; j < _boardSize; j++)
            {
                if (prev != board[i, j])
                {
                    winner = 0;
                }
            }

            if (winner != 0) return (winner);

            prev = board[0, i];
            winner = prev;
            for (int j = 1; j < _boardSize; j++)
            {
                if (prev != board[j, i])
                {
                    winner = 0;
                }
            }

            if (winner != 0) return (winner);
        }

        if ((_boardSize % 2) == 1)
        {
            prev = board[0, 0];
            winner = prev;
            for (int j = 1; j < _boardSize; j++)
            {
                if (prev != board[j, j])
                {
                    winner = 0;
                }
            }

            if (winner != 0) return (winner);

            prev = board[0, _boardSize - 1];
            winner = prev;
            for (int j = 1; j < _boardSize; j++)
            {
                if (prev != board[j, _boardSize - 1 - j])
                {
                    winner = 0;
                }
            }

            if (winner != 0)
            {
                return (winner);
            }
        }

        return (0);
    }

    public void GetPlayerInput(int row, int col)
    {
        if (_currentPlayer != _computerID)
        {
            _row = row;
            _col = col;
            CheckBoardIsEmpty();
        }
    }

    private void CheckBoardIsEmpty()
    {
        if ((_row >= 0 && _row < _boardSize) && (_col >= 0 && _col < _boardSize))
        {
            if (_board[_row, _col] == 0)
            {
                _board[_row, _col] = _currentPlayer;
                _currentPlayer = _currentPlayer * -1;

                _totalMoves++;
                Drow(_row, _col, _currentPlayer);
            }
        }
    }

    private void Drow(int row, int col, int currentPlayer)
    {
        for (int i = 0; i < _planes.Length; i++)
        {
            var currentPlane = _planes[i].GetComponent<Plane>();
            if (row == currentPlane.GetRow() && col == currentPlane.GetCol())
            {
                if (currentPlayer == 1)
                {
                    currentPlane.ChangeText("X");
                }
                else
                {
                    currentPlane.ChangeText("O");
                }
            }
        }
    }

    private void OpenEndingMenu(int winer)
    {
        var endingPanel = GameObject.Find("EndMenu").transform.GetChild(0);
        endingPanel.gameObject.SetActive(true);

        var winerText = endingPanel.GetChild(0).GetComponent<Text>();

        if (winer == 0)
        {
            winerText.text = "draw!!";
        }
        else if (winer == _computerID)
        {
            winerText.text = "You Lose!!";
        }
        else
        {
            winerText.text = "You Win!!";
        }
    }
}