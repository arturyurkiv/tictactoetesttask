﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour 
{

    public void OnChangeAiDifficult()
    {
        var dropDownValue = GetComponent<Dropdown>().value;

        switch (dropDownValue)
        {
            case 0:
                GameDifficult.AiDifficult = 5;
                break;
            case 1:
                GameDifficult.AiDifficult = 4;
                break;
            default:
                GameDifficult.AiDifficult = 3;
                break;
        }
    }
    

}
